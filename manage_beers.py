import os.path as op
from enum import Enum
import libbeer as lb
import libstored as lbst

# ~ CREATE TABLE IF NOT EXISTS books (
  # ~ BookID INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
  # ~ Title VARCHAR(100) NOT NULL, 
  # ~ SeriesID INT, AuthorID INT);

# ~| Name| Producer| Alc %| num| Type Basic| Type Full| Buy date| BBD


class SelOpt(Enum):
    ADDB = 0#add beer
    PRINTALLB = 1#print all beers
    EDITB = 2#edit beer
    BBD = 3#Best Before Date
    BUYDATE = 4
    DRINKB = 5#drink beer
    COUNTBEERS = 6#count how many different beers there are
    RANDOMBEER = 7#print a random beer not ageing
    FILTERPROD = 8#filter accordingly to a producer
    FILTERBTYPE = 9#filter accordingly to a beer type
    REMOVEB = 10#remove beer
    EXITM = 'q'#exit menu
    

def mainB():

    beerFile = "beer_db.txt"
    prodFile = "producer_db.txt"
    bTypeFile = "beerType_db.txt"
    contFile = "container_db.txt"
    drunkFile = "drunk_beers.txt"

    tmp = ""
    if not op.isfile(beerFile):
        tmp += "{} not found\n".format(beerFile)
    if not op.isfile(prodFile):
        tmp += "{} not found\n".format(prodFile)
    if not op.isfile(bTypeFile):
        tmp += "{} not found\n".format(bTypeFile)
    if not op.isfile(contFile):
        tmp += "{} not found\n".format(contFile)
    if not op.isfile(drunkFile):
        tmp += "{} not found".format(drunkFile)

    if tmp:
        print(tmp)
        raise FileNotFoundError

    beers = lb.beerLogUtils(beerFile,
                            prodFile,
                            bTypeFile,
                            contFile,
                            drunkFile)
    beers.readFromAllFiles()
    print(beers)

    #prodDB.printListOrdered()
    
    addb = SelOpt.ADDB.value
    printallb = SelOpt.PRINTALLB.value
    editb = SelOpt.EDITB.value
    bbd = SelOpt.BBD.value
    buydate = SelOpt.BUYDATE.value
    drinkb = SelOpt.DRINKB.value
    countBe = SelOpt.COUNTBEERS.value
    randomBe = SelOpt.RANDOMBEER.value
    filterProd = SelOpt.FILTERPROD.value
    filterBType = SelOpt.FILTERBTYPE.value
    removeb = SelOpt.REMOVEB.value
    exitm = SelOpt.EXITM.value

    
    fGoing = True
    while fGoing:
        choice = input('\nWhat do you want to do?\n'+
                       '[{}] add a beer\n'.format(addb)+
                       '[{}] print all the beers\n'.format(printallb)+
                       '[{}] edit a beer\n'.format(editb)+
                       '[{}] list beers by best before end\n'.format(bbd)+
                       '[{}] list beers by buy date\n'.format(buydate)+
                       '[{}] I drank a beer ;)\n'.format(drinkb)+
                       '[{}] how much stuff is there?\n'.format(countBe)+
                       '[{}] a random beer, not ageing\n'.format(randomBe)+
                       '[{}] filter for producer\n'.format(filterProd)+
                       '[{}] filter for beer type\n'.format(filterBType)+
                       '[{}] remove a beer\n'.format(removeb)+
                       '[{}] exit\n'.format(exitm)+
                       'Selection: ')

        print("")

        #exitm is 'q' therefore it would fail int(choice) check
        if choice == exitm:
            fGoing = 0
            continue
        
        try:
            choice = int(choice)
        except ValueError:
            print('Wrong selection\n')
            continue


        choice = int(choice)
        
        if choice == addb:
            beers.addCheckBeer()
        elif choice == printallb:
            beers.printOrderedBeerList()
        elif choice == editb:
            beers.editBeer()
        elif choice == bbd:
            num = int(input('\nHow many do you want to see? [0 => all] '))
            beers.printOrderedBy(num,'bestbefore')
        elif choice == buydate:
            num = int(input('\nHow many do you want to see? [0 => all] '))
            beers.printOrderedBy(num,'buydate')
        elif choice == drinkb:
            beers.editBeer('drink')
        elif choice == countBe:
            beers.countBeers()
            beers.countItems()
        elif choice == randomBe:
            beers.getRandomBeer()
        elif choice == removeb:
            beers.editBeer('delete')
        elif choice == filterProd:
            beers.filterAndPrint("producer")
        elif choice == filterBType:
            beers.filterAndPrint("beerType")
        #elif choice == exitm:
        #    fGoing = 0
        else:
            print('Wrong selection\n')



# Name-iD| Producer| Alc %| num| Container| Content| Type Basic| Type Full| Buy date| BBD| Ageing
#TABLEs: BEER, PRODUCER, TYPE (basic), Container
def main():

    beerDB = lbst.beer_stored()
    #fBeer = open("beer_db.txt","a+")
    #bTemp = list(zip(*[line.split() for line in fBeer]))
#    bTemp = {};
    with open('beer_db.txt', 'r') as file:
        #beerDB.beerTab = json.load(file)
        for line in file:
            bEntry = []
            line = line.strip()
            for item in line.split('\t'):
                bEntry.append(item)
            if bEntry[0]:
                beerDB.beerTab[bEntry[0]] = bEntry[1:]
        
    beerDB.printTab(beerDB.beerTab)
    
    # numCat = len(bTemp)
    # numBeer =
    # for item in range(0,numBeer):
    #     key_name = bTemp[0][item]#name
    #     beerDB.beerTab[key_name] = bTemp[range(1,numCat)][item]

    # print(beerDB.beerTab)

    #fProd = open("producer_db.txt","a+")
    #prodTab = zip(*[line.split() for line in fProd])
    with open('producer_db.txt', 'r') as file:
        for line in file:
            bEntry = []
            line = line.strip()
            for item in line.split('\t'):
                bEntry.append(item)
            if bEntry[0]:
                beerDB.prodTab[int(bEntry[0])] = bEntry[1:]
    
    beerDB.printTab(beerDB.prodTab)
    
    #fBType = open("beerType_db.txt","a+")
    #bTypeTab = zip(*[line.split() for line in fBType])
    with open('beerType_db.txt', 'r') as file:
        for line in file:
            bEntry = []
            line = line.strip()
            for item in line.split('\t'):
                bEntry.append(item)
            if bEntry[0]:
                beerDB.bTypeTab[int(bEntry[0])] = bEntry[1:]

    beerDB.printTab(beerDB.bTypeTab)
    
    #fContain =open("container_db.txt","a+")
    #containTab = zip(*[line.split() for line in fContain])
    with open('container_db.txt', 'r') as file:
        for line in file:
            bEntry = []
            line = line.strip()
            for item in line.split('\t'):
                bEntry.append(item)
            if bEntry[0]:
                beerDB.containTab[int(bEntry[0])] = bEntry[1:]

    beerDB.printTab(beerDB.containTab)

    addb = SelOpt.ADDB.value
    printallb = SelOpt.PRINTALLB.value
    editb = SelOpt.EDITB.value
    bbd = SelOpt.BBD.value
    buydate = SelOpt.BUYDATE.value
    drinkb = SelOpt.DRINKB.value
    removeb = SelOpt.REMOVEB.value
    exitm = SelOpt.EXITM.value

    
    fGoing = True
    while fGoing:
        choice = input('\nWhat do you want to do?\n'+
                       '[{}] add a beer\n'.format(addb)+
                       '[{}] print all the beers\n'.format(printallb)+
                       '[{}] edit a beer\n'.format(editb)+
                       '[{}] list beers by best before end\n'.format(bbd)+
                       '[{}] list beers by buy date\n'.format(buydate)+
                       '[{}] I drank a beer ;)\n'.format(drinkb)+
                       '[{}] remove a beer\n'.format(removeb)+
                       '[{}] exit\n'.format(exitm)+
                       'Selection: ')

        #exitm is 'q' therefore it would fail int(choice) check
        if choice == exitm:
            fGoing = 0
            continue
        
        try:
            choice = int(choice)
        except ValueError:
            print('Wrong selection\n')
            continue


        choice = int(choice)
        
        if choice == addb:
            beerDB.addBeer()
        elif choice == printallb:
            beerDB.printAllBeers()
        elif choice == editb:
            beerDB.editBeer()
        elif choice == bbd:
            num = int(input('\nHow many do you want to see? [0 => all] '))
            beerDB.orderbyDate(amount=num)
        elif choice == buydate:
            num = int(input('\nHow many do you want to see? [0 => all] '))
            beerDB.orderbyDate(amount=num,buydate='buydate')
        elif choice == drinkb:
            beerDB.editBeer('drink')
        elif choice == removeb:
            beerDB.editBeer('delete')
        #elif choice == exitm:
        #    fGoing = 0
        else:
            print('Wrong selection\n')

if __name__ == "__main__":
    mainB()
