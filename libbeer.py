import copy
import random


class Beer:
    """Class to manage one beer entity.
    
    :param name: Name of the beer
    :type name: str
    :param prod: Producer (code)
        (default is -1)
    :type prod: int
    :param collab: Collaboration (yes/no)
        (default is "")
    :type collab: str
    :param alc: Alcohol Percentage (%)
        (default is -1.0)
    :type alc: float
    :param nItems: Number of samples/items
        (default is -1)
    :type nItems: int
    :param contType: Container type (code)
            (default is -1)
    :type contType: int
    :param amt: Amount/Volume (ml)
            (default is -1)
    :type amt: int
    :param bType: Beer Type (code)
        (default is -1)
    :type bType: int
    :param typeDet: Detailed Beer Type
            (default is "")
    :type bType: str
    :param buydate: Buying Date
            (default is -1)
    :type buydate: int
    :param bbd: Best Before Date
            (default is -1)
    :type bbd: int
    :param ageing: Ageing (y/n)
            (default is "")
    :type ageing: str
    """
    def __init__(self,name,
                 prod=-1,
                 collab="",
                 alc=-1.0,
                 nItems=-1,
                 contType=-1,
                 amt=-1,
                 bType=-1,
                 typeDet="",
                 buydate=-1,
                 bbd=-1,
                 ageing=""):
        """Constructor of Class Beer
        
        :param name: Name of the beer
        :type name: str
        :param prod: Producer (code)
            (default is -1)
        :type prod: int
        :param collab: Collaboration (yes/no)
            (default is "")
        :type collab: str
        :param alc: Alcohol Percentage (%)
            (default is -1.0)
        :type alc: float
        :param nItems: Number of samples/items
            (default is -1)
        :type nItems: int
        :param contType: Container type (code)
            (default is -1)
        :type contType: int
        :param amt: Amount/Volume (ml)
            (default is -1)
        :type amt: int
        :param bType: Beer Type (code)
        (default is -1)
        :type bType: int
        :param typeDet: Detailed Beer Type
            (default is "")
        :type bType: str
        :param buydate: Buying Date
            (default is -1)
        :type buydate: int
        :param bbd: Best Before Date
            (default is -1)
        :type bbd: int
        :param ageing: Ageing (y/n)
            (default is "")
        :type ageing: str
        """
        self.name = name
        self.prod = int(prod)
        self.collab = collab
        self.alcohol = float(alc)
        self.nItems = int(nItems)
        self.contType = int(contType)
        self.amount = int(amt)
        self.bType = int(bType)
        self.typeDet = typeDet
        self.buydate = int(buydate)
        self.bbd = int(bbd)
        self.ageing = ageing

        return


    def __str__(self):
        """Printing function for Beer. String combining all parameters
        spaced out by tab.
        
        :return: string combining all parameters
        :rtype: str
        """
        outStr = "{}\t".format(self.name)
        outStr += "{}\t".format(self.prod)
        outStr += "{}\t".format(self.collab)
        outStr += "{}\t".format(self.alcohol)
        outStr += "{}\t".format(self.nItems)
        outStr += "{}\t".format(self.contType)
        outStr += "{}\t".format(self.amount)
        outStr += "{}\t".format(self.bType)
        outStr += "{}\t".format(self.typeDet)
        outStr += "{}\t".format(self.buydate)
        outStr += "{}\t".format(self.bbd)
        outStr += "{}".format(self.ageing)

        return outStr


    def getBStr(self):
        """To be removed? It returns the same as __str__
        
        """
        beerStr = self.__str__()
        return beerStr
        
    
    def setName(self,name):
        """Setter for name of the beer.
        
        :param name: name of beer
        :type name: str
        """
        self.name = name
        return


    def setProd(self,name):
        """Setter for producer code.
        
        :param prod: code of producer
        :type prod: int
        """
        self.prod = name
        return


    def setCollab(self,collab):
        """Setter for whether it's a collaboration-beer 'y' or not 'n'
        
        :param collab: collab-beer? ('y' or 'n')
        :type collab: str
        """
        if collab == "y" or collab == "n":
            self.collab = collab
        else:
            print("We need \"y\" or \"n\"")
            raise ValueError
        
        return


    def setAlcohol(self,alc):
        """Setter for alcohol percentage.
        
        :param alc: alcohol percentage (%)
        :type alc: float
        """
        try:
            self.alcohol = float(alc)
        except ValueError:
            print("We need a number here ;)")
            raise
        
        return

    
    def setnItems(self,num):
        """Setter for number of items.
        
        :param num: number of items
        :type num: int
        """
        try:
            self.nItems = int(num)
        except ValueError:
            print("We need a number here ;)")
            raise

        return


    def setContType(self,cType):
        """Setter for container type code.
        
        :param num: number of items
        :type num: int
        """
        contType = str(cType)
        if contType == "1" or contType == "2":
            self.contType = int(contType)
        else:
            print("We need \"0\" or \"1\"")
            raise ValueError
        
        return


    def setAmount(self,amt):
        """Setter for amount of content.
        
        :param amt: content amount (ml)
        :type amt: int
        """
        try:
            self.amount = int(amt)
        except ValueError:
            print("We need a number here ;)")
            raise
    
        return


    def setBeerType(self,btype):
        """Setter for beer type code.
        
        :param btype: beer type code
        :type btype: int
        """
        try:
            self.bType = int(btype)
        except ValueError:
            print("We need a number here ;)")
            raise
    
        return


    def setBTypeDet(self,detail):
        """Setter for beer type detailed.
        
        :param detail: beer type detail
        :type detail: str
        """
        det = detail
        if not detail:
            det = "--"

        self.typeDet = det
        return


    def setBuyDate(self,buy):
        """Setter for buy date.
        
        :param buy: buy date (YYYYMMDD)
        :type buy: int
        """
        bDate = 0
        try:
            bDate = int(buy)
        except ValueError:
            print("We need a number here ;)")
            raise

        if (bDate/10000000) == 0:
            print("We need a date in the format YYYYMMDD")
            raise ValueError
        else:
            self.buydate= bDate
        return


    def setBBD(self,best):
        """Setter for best before date.
        
        :param best: best before date (YYYYMMDD)
        :type best: int
        """
        bestD = 0
        try:
            bestD = int(best)
        except ValueError:
            print("We need a number here ;)")
            raise

        if (bestD/10000000) == 0:
            print("We need a date in the format YYYYMMDD")
            raise ValueError
        else:
            self.bbd = bestD
        return


    def setAgeing(self,age):
        """Setter for whether it's a beer for aeging. Only 'y' or 'n'
        
        :param age: ageing ('y' or 'n')
        :type age: str
        """
        if age == "y" or age == "n":
            self.ageing = age
        else:
            print("We need \"y\" or \"n\" ;)")
            raise ValueError
        
        return


    def getName(self):
        """Getter for name of the beer.
        
        :return: name of beer
        :rtype: str
        """
        return self.name


    def getProd(self):
        """Getter for producer code.
        
        :return: code of producer
        :rtype: int
        """
        return self.prod


    def getCollab(self):
        """Getter for whether it's a collaboration-beer 'y' or not 'n'
        
        :return: collab-beer? ('y' or 'n')
        :rtype: str
        """
        return self.collab


    def getAlcohol(self):
        """Getter for alcohol percentage.
        
        :return: alcohol percentage (%)
        :rtype: float
        """
        return self.alcohol


    def getnItems(self):
        """Getter for number of items.
        
        :return: number of items
        :rtype: int
        """
        return int(self.nItems)


    def getContType(self):
        """Getter for container type code.
        
        :return: number of items
        :rtype: int
        """
        return self.contType

    
    def getAmount(self):
        """Getter for amount of content.
        
        :return: content amount (ml)
        :rtype: int
        """
        return self.amount


    def getBeerType(self):
        """Getter for beer type code.
        
        :return: beer type code
        :rtype: int
        """
        return self.bType


    def getBTypeDet(self):
        """Getter for beer type detailed.
        
        :return: beer type detail
        :rtype: str
        """
        return self.typeDet


    def getBuyDate(self):
        """Getter for buy date.
        
        :return: buy date (YYYYMMDD)
        :rtype: int
        """
        return self.buydate


    def getBBD(self):
        """Getter for best before date.
        
        :return: best before date (YYYYMMDD)
        :rtype: int
        """
        return self.bbd


    def getAgeing(self):
        """Getter for whether it's a beer for aeging. Only 'y' or 'n'
        
        :return: ageing ('y' or 'n')
        :rtype: str
        """
        return self.ageing


    def getRawVars(self):
        """Return the codes of producer, container type and beer type.
        This because those are the only parameters which are in a code
        format. These codes then need to be used in other lists to get
        the real strings.

        :return: [producer code, container type code, beer type code]
        :rtype: list[]*3
        """
        return [self.getProd(),self.getContType(),self.getBeerType()]


    def returnBeerFunc(self,attr):
        """Return one of the Beer functions based on the string passed as
        attr.
        
        :param attr: function name
        :type attr: str
        :return: function object
        :rtype: object
        """
        return getattr(Beer,attr)(self)


class addonList:
    """This is a base class to manage lists of key-value.
    Every list is connected to an external file where information are read
    from and written to.
    Right now it assumes that the key carries information that needs to be
    printed and exposed to the external world.

    :param fileToWrite: external file to store information
        (default is "")
    :type fileToWrite: str
    """
    def __init__(self,fileToWrite=""):
        """Contructor of class addonList
        
        :param fileToWrite: external file to store information
            (default is "")
        :type fileToWrite: str
        """
        self.dictList = {}
        self.fileName=fileToWrite
        return


    def __str__(self):
        listKeys = sorted(self.dictList.keys())

        tmpStr = ""
        for key in listKeys:
            val = self.dictList[key]
            tmpStr += "{}\t{}\n".format(key,val.__str__())
            
        return tmpStr


    def setFileName(self,fileName):
        """Setter for storage file.
        
        :param fileName: external file to store information
        :type fileName: str
        """
        self.fileName=fileName
        return
    
    
    def getLStr(self):
        tmpStr = self.__str__()
        return tmpStr


    def getVal(self,key):
        kstr = str(key)
        return self.dictList[kstr]


    def setVal(self,key,val):
        kstr = str(key)
        self.dictList[kstr] = val
        return


    def getListKeys(self):
        listKeys = copy.deepcopy([*self.dictList.keys()])
        return listKeys
    

    def isItInList(self,item):
        #if item is an object, the comparison could fail even if it
        #shouldn't.
        values = [*self.dictList.values()]
        if item in values:
            return True
        else:
            return False
        return False


    def isKeyInList(self,key):
        listKeys = [*self.dictList.keys()]
        kstr = str(key)
        if kstr in listKeys:
            return True
        else:
            return False
        return False


    def printListOrdered(self):
        '''This is just for 2 columns lists
        
        '''
        keys = [*self.dictList.keys()]
        values = [*self.dictList.values()]
        itemsOrdered = [[k,v] for v,k in sorted(zip(values,keys))]

        for item in itemsOrdered:
            print(str(item[0]) + "\t" + str(item[1]))
        return


    # Ask, display a list and take a value
    def askShowTake(self, question, old = ''):
        self.printListOrdered()

        questAll = "(write the corresponding number. If not already there, write -1)\n\n" + question

        item = ''
        if (not old) or (old == -1):
            item = input(questAll)
        else:
            item = input(questAll + '\n(Old value: %s)\n' % old)
            if not item:
                return int(old)

        listKeys = [*self.dictList.keys()]
        #print(listKeys)#debug

        if item in listKeys:
            return int(item)
        
        newKey = self.addItem(item)
        
        return newKey
    
        
    def addItem(self,item):
        listKeys = sorted(self.dictList.keys())
        listValues = [*self.dictList.values()]

        if (item in listValues):
            print("Item already inside the list.")
            return self.dictList[item]
        else:
            for i in listKeys:#probably this can be improved
                              #since the list is ordered
                              #however this method works also when
                              #the list has 'holes' in the middle
                j = str(int(i)+1)
                if not (j in listKeys):
                    self.dictList[j] = str(input("Write the addition: "))
                    return j
                
            if len(listKeys) == 0:#this happens only the first time
                                  #(file empty)
                self.dictList[0] = [str(input("Write the addition: "))]
                return 0

        return


    def countEntries(self):
        """Return the number of entries in the addonList/dictionary
        :return: number of entries in/length of dictionary
        :rtype: int
        """
        return int(len(self.dictList))


    def readFromFile(self,fileName=""):
        #can be used only with an empty dictList
        #moreover: it works only with 2 columns-files
        readFile = fileName
        if not fileName:
            readFile = self.fileName
        
        with open(readFile, 'r') as file:
            for line in file:
                bEntry = []
                line = line.strip()
                for item in line.split('\t'):
                    bEntry.append(item)
                if bEntry[0]:
                    self.dictList[bEntry[0]] = bEntry[1]

        return


    def writeToFile(self,fileName=""):
        #it works only with 2 columns-files
        writeFile = fileName
        if not fileName:
            writeFile = self.fileName
            
        with open(writeFile, 'w') as f:
            f.write(self.__str__())

        print('Done with {}\n'.format(writeFile))
        return

    
class beerList(addonList):
    
    def __str__(self):
        #this is not so different from the function in addonList.
        #however it doesn't print the key, since key and beer name are
        #the same.
        listKeys = sorted(self.dictList.keys())

        tmpStr = ""
        for key in listKeys:
            val = self.dictList[key]
            tmpStr += "{}\n".format(val.__str__())
            
        return tmpStr

    
    def addBeer(self,beer):
        key = beer.getName()
        self.dictList[key] = beer
        return


    def removeBeer(self,key):
        self.dictList.pop(key,None)
        return


    def editBeer(self,oldKey,beer):
        oldBeer = self.dictList[oldKey]

        newName = beer.getName()
        if oldKey == newName:#just change the corresponding beer obj
            self.dictList[oldKey] = beer
        else:#change also the key => delete the old one
            self.removeBeer(oldKey)
            self.dictList[newName] = beer

        return
    

    def readFromFile(self,fileName=""):
        #can be used only with an empty dictList
        readFile = fileName
        if not fileName:
            readFile = self.fileName
        
        with open(readFile, 'r') as file:
            for line in file:
                bEntry = []
                line = line.strip()
                for item in line.split('\t'):
                    bEntry.append(item)
                if bEntry[0]:
                    beer = Beer(*bEntry)
                    self.addBeer(beer)
                
        return


    def orderByAttr(self,attr):
        '''attr is the function you want to use to extract the values
        you're interested in.
        return the list of keys ordered according to attr
        '''

        listKeys = [*self.dictList.keys()]
        
        listValues = []
        for key in listKeys:
            listValues.append(self.dictList[key].returnBeerFunc(attr))

        keysOrdered = [key for _,key in sorted(zip(listValues,listKeys))]

        return keysOrdered


    def countItems(self):
        """Return the total number of items (bottles and cans) in the
        collection.
        :return: number of items (bottles and cans)
        :rtype: int
        """
        totItems = 0
        listKeys = [*self.dictList.keys()]
        for key in listKeys:
            nItems = self.dictList[key].getnItems()
            totItems += nItems

        return int(totItems)


    #maybe it should take a list as parameter
    #so that we could pass it the list already filtered (e.g. best before)
    def filterBeers(self,filtP,valP):
        """It returns a lists of keys according to the parameter filtP if
        equal to valP.
        :param filtP: parameter of Beer to use for filtering
        :type param: str
        :param valP: value of the parameter to be filtered
        :type valP: str
        :return: list of filtered keys
        :rtype: list []
        """
        listKeys = [*self.dictList.keys()]
        fKeys = []
        func = ""

        funcCases = {"ageing" : "getAgeing",
                     "producer" : "getProd",
                     "beerType" : "getBeerType"
        }

        func = funcCases[filtP]
        
        for key in listKeys:
            par = str(self.dictList[key].returnBeerFunc(func))
            if par == valP:
                fKeys.append(key)
            
        return fKeys


    def getStrSubPart(self,listKeys):
        keysOrdered = sorted(listKeys)

        tmpStr = ""
        for key in keysOrdered:
            val = self.dictList[key]
            tmpStr += "{}\n".format(val.__str__())
            
        return tmpStr
    
    
class prodList(addonList):
    #could we maybe set the file names where to write?
    pass


class bTypeList(addonList):
    pass


class contTypeList(addonList):
    pass
        

#####################################
#####################################
#####################################


class beerLogUtils:
    def __init__(self,fileBeers="",fileProd="",fileType="",
                 fileCont="",fileDrunk=""):
        self.beersObj = beerList(fileBeers)
        self.prodObj = prodList(fileProd)
        self.bTypeObj = bTypeList(fileType)
        self.containObj = contTypeList(fileCont)
        self.drunkObj = beerList(fileDrunk)
        return


    def __str__(self):
        tmpstr = self.beersObj.__str__() + "\n"
        tmpstr += self.prodObj.__str__() + "\n"
        tmpstr += self.bTypeObj.__str__() + "\n"
        tmpstr += self.containObj.__str__() + "\n"
        tmpstr += self.drunkObj.__str__()

        return tmpstr

    
    def setBeersFile(self,fileName):
        self.beersObj.setFileName(fileName)
        return


    def setProdFile(self,fileName):
        self.prodObj.setFileName(fileName)
        return

    
    def setbTypeFile(self,fileName):
        self.bTypeObj.setFileName(fileName)
        return


    def setContainFile(self,fileName):
        self.containObj.setFileName(fileName)
        return


    def setDrunkFile(self,fileName):
        self.drunkObj.setFileName(fileName)
        return


    def getBeerList(self):
        return self.beersObj


    def getProdList(self):
        return self.prodObj

    
    def getBeerTypesList(self):
        return self.bTypeObj

    
    def getContainerList(self):
        return self.containObj


    def getDrunkList(self):
        return self.drunkObj


    def getBeersStr(self):
        return self.beersObj.__str__()

    
    def getProdsStr(self):
        return self.prodObj.__str__()

    
    def getBeerTypesStr(self):
        return self.bTypeObj.__str__()

    
    def getContainerStr(self):
        return self.containObj.__str__()


    def getDrunkStr(self):
        return self.drunkObj.__str__()


    def readBeersFile(self):
        self.beersObj.readFromFile()
        return


    def readProdFile(self):
        self.prodObj.readFromFile()
        return


    def readBeerTypesFile(self):
        self.bTypeObj.readFromFile()
        return


    def readContainerFile(self):
        self.containObj.readFromFile()
        return


    def readDrunkFile(self):
        self.drunkObj.readFromFile()
        return


    def readFromAllFiles(self):
        self.readBeersFile()
        self.readProdFile()
        self.readBeerTypesFile()
        self.readContainerFile()
        self.readDrunkFile()
        return


    def writeBeersToFile(self):
        self.beersObj.writeToFile()
        return


    def writeProdToFile(self):
        self.prodObj.writeToFile()
        return


    def writeBeerTypesToFile(self):
        self.bTypeObj.writeToFile()
        return


    def writeContainerToFile(self):
        self.containObj.writeToFile()
        return


    def writeDrunkToFile(self):
        self.drunkObj.writeToFile()
        return


    def writeToAllFiles(self):
        self.writeBeersToFile()
        self.writeProdToFile()
        self.writeBeerTypesToFile()
        self.writeContainerToFile()
        self.writeDrunkToFile()
        return


    def getProcessedVars(self,prodRaw,contRaw,btypeRaw):
        '''Return the names/strings related to producer, container type
        and beer type.
        '''
        #to check that they are digits, change to str and use isdigit
        pstr = str(prodRaw)
        cstr = str(contRaw)
        bstr = str(btypeRaw)
        if not (pstr.isdigit() and
                cstr.isdigit() and
                bstr.isdigit()):
            print("Some of the input variables are not numbers")
            raise ValueError
            
            
        prod = self.prodObj.getVal(prodRaw)
        cont = self.containObj.getVal(contRaw)
        btype = self.bTypeObj.getVal(btypeRaw)
        return [prod,cont,btype]


    def printOrderedBeerList(self):
        listKeys = sorted(self.beersObj.getListKeys())
        self.printCompletedList(listKeys)
        return
    
    
    def printCompletedList(self,listKeys):
        #only for beerList type!
        #we should get the producer etc and print those
        #instead of numbers
        for key in listKeys:
            beer = self.beersObj.getVal(key)
            listVars = beer.getRawVars()
            [prod,cont,btype] = self.getProcessedVars(*listVars)
            tmpStr = "{:<18}\t".format(beer.getName())
            tmpStr += "{}\t".format(prod)#beer.getProd()
            tmpStr += "Collab: {}\t".format(beer.getCollab())
            tmpStr += "{}%\t".format(beer.getAlcohol())
            tmpStr += "{} ".format(beer.getnItems())
            tmpStr += "{}/s\t".format(cont)#beer.getContType()
            tmpStr += "{} ml\t".format(beer.getAmount())
            tmpStr += "{}, ".format(btype)#beer.getBeerType()
            tmpStr += "{}\t".format(beer.getBTypeDet())
            tmpStr += "Buy: {}\t".format(beer.getBuyDate())
            tmpStr += "BB: {}\t".format(beer.getBBD())
            tmpStr += "Ageing: {}".format(beer.getAgeing())
            print(tmpStr)
        #or should we return the whole string of all the listed beers?
        #print(self.beersObj)
        return


    def printCompletedDrunk(self,listKeys):
        #only for drunk database
        #we don't print all the values because some are not needed
        #we should get the producer etc and print those
        #instead of numbers
        tmpStr = ''
        for key in listKeys:
            beer = self.drunkObj.getVal(key)
            listVars = beer.getRawVars()
            [prod,cont,btype] = self.getProcessedVars(*listVars)
            tmpStr += "{:<18}\t".format(beer.getName())
            tmpStr += "{}\t".format(prod)#beer.getProd()
            tmpStr += "Collab: {}\t".format(beer.getCollab())
            tmpStr += "{}%\t".format(beer.getAlcohol())
            tmpStr += "{} ".format(beer.getnItems())
            tmpStr += "{}/s\t".format(cont)#beer.getContType()
            tmpStr += "{} ml\t".format(beer.getAmount())
            tmpStr += "{}, ".format(btype)#beer.getBeerType()
            tmpStr += "{}\t".format(beer.getBTypeDet())
            #tmpStr += "Buy: {}\t".format(beer.getBuyDate())
            #tmpStr += "BB: {}\t".format(beer.getBBD())
            tmpStr += "Ageing: {}\n".format(beer.getAgeing())

        print(tmpStr)
        #or should we return the whole string of all the listed beers?
        #print(self.beersObj)
        return

    
    # Ask and take a value
    def askTake(self,question, old = ''):
        tmp = ""
        questAll = "\n" + question
        if (not old) or (old == -1):
            tmp = input(questAll)
        else:
            tmp = input(str(questAll) + ' (Old value: {}) '.format(old))
            if not tmp:
                tmp = old
        
        return tmp


    def addBeerToDB(self, key = '', old = None):

        key_name = ""

        if old is not None:
            oldB = old
        else:
            oldB = Beer("")
        
        #If the key is provided, probably we want to edit the beer
        if not key:
            key_name = self.askTake('Name: ')
        else:
            key_name = self.askTake('Name: ', oldB.getName())

        #If the key is already in the list, then we just update the number
        #of items
        if self.beersObj.isItInList(key_name):
            beer = self.beersObj.getVal(key_name)
            new_in = int(input( ('The beer is already registered. '+
                                 'How many items do you want to add? ') ))
            old_amount = beer.getAmount()
            new_in += old_amount
            beer.setAmount(new_in)
            self.beersObj.editBeer(beer.getName(),beer)
            self.writeToAllFiles()
            return

        newBeer = Beer(key_name)
        #Producer
        tmp = self.prodObj.askShowTake('Producer: ', oldB.getProd())
        newBeer.setProd(tmp)
        #Collaboration
        tmp = self.askTake('Collaboration (Y/N): ', oldB.getCollab())
        newBeer.setCollab(tmp)
        #Alcohol
        tmp = float(self.askTake('Alcohol (%): ', oldB.getAlcohol()))
        newBeer.setAlcohol(tmp)
        #Number of Items
        tmp = int(self.askTake('Amount items: ', oldB.getnItems()))
        newBeer.setnItems(tmp)
        #Type of Container
        tmp = self.containObj.askShowTake('Type Container: ',
                                          oldB.getContType())
        newBeer.setContType(tmp)
        #Amount/Volume
        tmp = int(self.askTake('Content (ml): ', oldB.getAmount()))
        newBeer.setAmount(tmp)
        #Beer Type
        tmp = self.bTypeObj.askShowTake('Type (Base): ',oldB.getBeerType())
        newBeer.setBeerType(tmp)
        #Beer Type Detail
        tmp = self.askTake('Type (Detail): ', oldB.getBTypeDet())
        newBeer.setBTypeDet(tmp)
        #Buy Date
        tmp = int(self.askTake('Buy Date (YYYYMMDD): ', oldB.getBuyDate()))
        newBeer.setBuyDate(tmp)
        #Best Before Date
        tmp = int(self.askTake('Best Before Date (YYYYMMDD): ',
                               oldB.getBBD()))
        newBeer.setBBD(tmp)
        #Ageing
        tmp = self.askTake('Ageing (Y/N): ', oldB.getAgeing())
        newBeer.setAgeing(tmp)

        self.beersObj.addBeer(newBeer)
        self.writeToAllFiles()
        return


    def checkDrunkFirst(self):
        """Function that first ask you for beer name and then checks
        inside the drunk beers database. If the beer you're trying
        to add is already there, it'll return the key and the (old) beer.
        Otherwise only the key (and None instead of the beer).

        :return: [key, beer/None]
        :rtype: list []
        """
        kBeer = ''#key of the beer
        allSearch = False
        drunkKeys = self.drunkObj.getListKeys()
        
        while True:
            inBeer = input('Beer Name: ').lower()#make all lower case
            ans = input('Do you want to search specifically '+
                        'for ALL those words? (Y/n) ')

            if ans == 'y' or ans == '':
                allSearch = True
            else:
                allSearch = False
            print('Searching in Drunk database...\n')

            inBeer = inBeer.split()
            beersFound = []
            if allSearch:#check that all the input words are in key
                for key in drunkKeys:#we compare to lower case keys
                    if all(x in key.lower() for x in inBeer):
                        beersFound.append(key)
                self.printCompletedDrunk(beersFound)
            else:#check that any input words are in key
                for key in drunkKeys:#we compare to lower case keys
                    if any(x in key.lower() for x in inBeer):
                        beersFound.append(key)
                self.printCompletedDrunk(beersFound)
                        

            kBeer = input('\nCopy the name of the beer '+
                          'or write a new name\n'+
                          '(leave empty to repeat search): ')
            print('\n')
            
            if kBeer:
                break

        drunkB = None
        if kBeer in drunkKeys:
            drunkB = self.drunkObj.getVal(kBeer)
        
        return (kBeer,drunkB)


    def addCheckBeer(self):
        """Function to add new beers. First it'll check if the beer is
        already in the drunk beers database and then it'll proceed with
        the adding part.
        """
        key,beer = self.checkDrunkFirst()
        self.addBeerToDB(key,beer)
        return

    #To edit parameters of a beer already registered
    def editBeer(self, action=''):
        flag = True
        tmp = ''
        word = 'edit'
        
        if action:
            word = action
        
        while flag:
            self.printOrderedBeerList()
            tmp = input('\nWrite the name of the beer you want to %s: ' % word)

            if not(self.beersObj.isKeyInList(tmp)):
                print('The beer is not registered yet')
            else:
                break
                
        
        if action == 'drink':
            nItems = int(input('\nHow many have you drunk? '))
            oldbeer = self.beersObj.getVal(tmp)
            print(oldbeer)
            new_nItems = int(oldbeer.getnItems()) - nItems
            print("\nRemaining number: {}".format(new_nItems))
            
            if new_nItems != 0:
                oldbeer.setnItems(new_nItems)
                self.beersObj.setVal(tmp,oldbeer)
                self.writeToAllFiles()
                return
            else:
                drunk = copy.deepcopy(oldbeer)
                print('Removing {}\n'.format(tmp))
                self.beersObj.removeBeer(tmp)
                self.drunkObj.addBeer(drunk)
                self.writeToAllFiles()
                return
        #else the beer is going to be deleted or edited
            
        
        oldbeer = copy.deepcopy(self.beersObj.getVal(tmp))
        self.beersObj.removeBeer(tmp)

        #delete beer
        if action == 'delete':#delete
            self.writeToAllFiles()
            return

        #edit the beer = add a new keeping an eye on the old one
        self.addBeerToDB(key=tmp, old=oldbeer)
        return
    
        
    #default behaviour = order by best before date
    def printOrderedBy(self, nDisplay = 0, attr = ''):

        funcAttr = None
        keysOrdered = []
        if attr == 'buydate':
            funcAttr = 'getBuyDate'
        elif attr == 'bestbefore':
            funcAttr = 'getBBD'
        else:
            print('You need to tell us what to order accordingly.')
            print('Right now you can choose between:')
            print('\'buydate\' and \'bestbefore\'')
            return

        keysOrdered = self.beersObj.orderByAttr(funcAttr)

        ntoprint = len(keysOrdered)
        if nDisplay:
            ntoprint = nDisplay

        keysToPrint = keysOrdered[:ntoprint]

        self.printCompletedList(keysToPrint)
        return


    def countBeers(self):
        """Print the amount of entries in the beersObj/list of unique
        beers. It does NOT print the total number of bottles/cans in the
        collection.
        """
        numB = self.beersObj.countEntries()
        print("Number of unique beers: {}".format(numB))
        return


    def countItems(self):
        """Print the total number of items (cans and bottles) in the
        collection.
        """
        totItems = self.beersObj.countItems()
        print("Total amount of items (cans+bottles): {}".format(totItems))
        return


    def getRandomBeer(self):
        """Get a random beer
        """
        keysList = self.beersObj.filterBeers("ageing","n")
        tot = len(keysList)
        if tot == 0:
            print("No beers satisfy your filtering.")
            return
        randN = random.randint(0,len(keysList)-1)
        print(self.beersObj.getVal(keysList[randN]))
        return


    def filterAndPrint(self,filtering):

        listsDict = {
            "producer": self.prodObj,
            "beerType": self.bTypeObj
        }

        printedList = listsDict[filtering]

        printedList.printListOrdered()

        itemFilter = input(
            "Write the number of the item you want to use to filter "+
            "the beer list: "
        )

        valFilter = printedList.getVal(itemFilter)
        
        listKeys = self.beersObj.filterBeers(filtering,itemFilter)

        print('')
        self.printCompletedList(listKeys)
        print('\nTotal amount of {} beers: {}\n'.format(valFilter,
                                                        len(listKeys)))

        return
