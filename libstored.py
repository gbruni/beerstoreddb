class beer_stored:
    """Obsolete class used to store beers.
    This library was the first attempt to store beers.
    There's no structure at all involving objects, dictionaries and related
    functions.
    """
    def __init__(self):
        self.beerTab = {}
        self.prodTab = {}
        self.bTypeTab = {}
        self.containTab = {}
        return

    
    # Ask and take a value
    def askTake(self, question, old = ''):
        tmp = ""
        if not old:
            tmp = input(question)
        else:
            tmp = input(question + ' (Old value: %s) ' % old)
            if not tmp:
                tmp = old
        
        return tmp

    
    # Ask, display a list and take a value
    def askShowTake(self, question, tab, old = ''):
        self.printOrderedTab(tab)

        questAll = question + "(write the corresponding number. If not already there, write -1)" 

        item = ''
        if not old:
            item = input(questAll)
        else:
            item = input(questAll + '\n(Old value: %s)\n' % old)
            if not item:
                item = old

        
        #item = input()

        if item.isdigit():
            item = int(item)

        listKeys = sorted(tab.keys())

        if (item in listKeys):
            return int(item)
        else:
            for i in listKeys:#probably this can be improved
                              #since the list is ordered
                              #however this method works also when
                              #the list has 'holes' in the middle
                j = int(i)+1
                if not (j in listKeys):
                    tab[j] = [str(input("Write the addition: "))]
                    return j
                
            if len(listKeys) == 0:#this happens only the first time
                                  #(file empty)
                tab[0] = [str(input("Write the addition: "))]
                return 0
            #improved?
            #j = int(listKeys[-1]) + 1
            #tab[j] = input("Write the addition: ")
            #return j
        
        return "Err"


    # Ask, take a value (date) and format in a date
    def askTakeDate(self, question, old = ''):
        if not old:
            print(question)
        else:
            print(question + ' (Old value: %s) ' % old)
        tmp = input()
        y = int(tmp[0:4])
        m = int(tmp[4:6])
        d = int(tmp[6:8])
        return dt.date(y,m,d)


    def getValue(self, key, index):
        vec = self.beerTab[key].copy()
        return vec[index]


    #default behaviour = order by best before date
    def orderbyDate(self, amount = 0, buydate = ''):
        index = 9#best before date
        if buydate:
            index = 8#buy date
        
        listKeys = list(self.beerTab.keys())

        beers = []
        dates = []
        
        for key in listKeys:
            beer = self.beerTab[key].copy()
            
            beers.append(key)
            dates.append(beer[index])
            #print(key, beer)
            
        beersOrdered = [beer for _,beer in sorted(zip(dates,beers))]

        #to decide how many beers to print
        beerstoprint = len(beersOrdered)
        if amount:
            beerstoprint = amount
            
        cnt = 0#counter to stop printing
        
        for key in beersOrdered:

            if cnt == beerstoprint:
                break
            cnt += 1
            print(cnt)

            beer = self.beerTab[key].copy()
            #print(key)
            #print(beer)

            beer[0] = self.searchValue(self.prodTab, int(beer[0]), 0)
            beer[4] = self.searchValue(self.containTab, int(beer[4]), 0)
            beer[6] = self.searchValue(self.bTypeTab, int(beer[6]), 0)

            beer.insert(0, key)
    
            print('\t'.join(str(i) for i in beer) + '\n')


        self.printTab(self.beerTab)
        return
    

    def searchValue(self, tab, key, index = 0):
        return tab[key][index]

    
    def printAllBeers(self):
        listKeys = list(self.beerTab.keys())

        beers = []
        
        for key in listKeys:
            beer = self.beerTab[key].copy()

            beer[0] = self.searchValue(self.prodTab, int(beer[0]), 0)
            beer[4] = self.searchValue(self.containTab, int(beer[4]), 0)
            beer[6] = self.searchValue(self.bTypeTab, int(beer[6]), 0)

            beer.insert(0, key)
            
            beers.append(beer)

        print('\nAll your beers, messere:')
            
        for beer in beers:
            print('\t'.join(str(i) for i in beer) + '\n')

        return

    
    def printTab(self, tab):
        listKeys = list(tab.keys())
        for item in sorted(listKeys):
            print(str(item) + "\t" +" ".join(str(i) for i in tab[item])) 
        return


    #The tab is ordered according to one of the values, not the key
    #We assume that the value to order according to is the first [0]
    #of the "value" part
    def printOrderedTab(self, tab):
        listKeys = list(tab.keys())

        beers = []
        values = []
        
        for key in listKeys:
            beer = tab[key].copy()
            #print(beer[0])
            beers.append(key)
            values.append(beer[0])
            
        itemsOrdered = [beer for _,beer in sorted(zip(values,beers))]

        for item in itemsOrdered:
            #print(str(item) + "\t" +" ".join(str(i) for i in itemsOrdered[item]))
            print(str(item) + "\t" + str(tab[item][0])) 
        return

    
    def writeOneTabtoFile(self, tab, txtFile):
        
        print('Writing to %s\n' % txtFile)
        self.printTab(tab)
        
        with open(txtFile, 'w') as f:
            #json.dump(f,tab)
            listKeys = list(tab.keys())
            for item in sorted(listKeys):
                print(item)
                print(tab[item])
                f.write(str(item) + "\t" +
                        "\t".join(str(i) for i in tab[item]) + '\n')
                

        print('Done\n')
        return

        
    def writeAllTabsToFiles(self):
        self.writeOneTabtoFile(self.beerTab, 'beer_db.txt')
        self.writeOneTabtoFile(self.prodTab, 'producer_db.txt')
        self.writeOneTabtoFile(self.bTypeTab, 'beerType_db.txt')
        self.writeOneTabtoFile(self.containTab, 'container_db.txt')
        
        return

    
    def addBeerToList(self, key, inBeer):
        #if a beer is already inside the list
        #I just need to add to the total amount
        #amount = 0
        #if (key in beerTab.keys() ):
        #    amount = self.getValue(key, 7) + 1
        #    inBeer[7] = amount
        #else:
        self.beerTab[key] = inBeer

        return 
    
    
    def addBeer(self, key = '', old = ''):
        tmpBeer = []

        if not key:
            key_name = self.askTake('Name: ')
        else:
            key_name = self.askTake('Name: (Old: %s) ' % str(key) )

        if (key_name in self.beerTab.keys() ):
            amount = int(self.getValue(key_name, 3))
            new_in = int(input( ('The beer is already registered. How many items do you want to add? ') ))
            self.beerTab[key_name][3] = amount + new_in
            self.writeAllTabsToFiles()
            return

        oldB = ['']*11
        if old:
            oldB = old
        
        #0
        tmpBeer.append(self.askShowTake('Producer: ', self.prodTab, oldB[0]))
        #1
        tmpBeer.append(self.askTake('Collaboration (Y/N): ', oldB[1]))
        #2
        tmpBeer.append(float(self.askTake('Alcohol (%): ', oldB[2])))
        #3
        tmpBeer.append(int(self.askTake('Amount items: ', oldB[3])))
        #4
        tmpBeer.append(int(self.askShowTake('Type Container: ', self.containTab, oldB[4])))
        #5
        tmpBeer.append(int(self.askTake('Content (ml): ', oldB[5])))
        #6
        tmpBeer.append(self.askShowTake('Type (Base): ', self.bTypeTab, oldB[6]))
        #7
        tmpBeer.append(self.askTake('Type (Detail): ', oldB[7]))
        #8
        #tmpBeer.append(self.askTakeDate('Buy Date (YYYYMMDD): ', oldB[7]))
        tmpBeer.append(int(self.askTake('Buy Date (YYYYMMDD): ', oldB[8])))
        #9
        #tmpBeer.append(self.askTakeDate('Best Before Date (YYYYMMDD): ', oldB[8]))
        tmpBeer.append(int(self.askTake('Best Before Date (YYYYMMDD): ', oldB[9])))
        #10
        tmpBeer.append(self.askTake('Ageing (Y/N): ', oldB[10]))

        self.addBeerToList(key_name, tmpBeer)
        self.writeAllTabsToFiles()

        
    #To edit parameters of a beer already registered
    def editBeer(self, delete=''):
        flag = True

        tmp = ''

        word = 'edit'
        if delete:
            word = delete
        
        while flag:
            self.printTab(self.beerTab)
            tmp = input('Write the name of the beer you want to %s: ' % word)

            if tmp in list(self.beerTab.keys()):
                break

            print('The beer is not registered yet')
        
        
        if delete == 'drink':
            amount = int(input('How many have you drunk? '))
            oldbeer = self.beerTab[tmp]
            new_amount = int(oldbeer[3]) - amount
            
            if not (new_amount == 0):
                oldbeer[3] = new_amount
                self.writeAllTabsToFiles()
                return
            else:
                txtFile = 'drunk_beers.txt'
                print('Writing to %s\n' % txtFile)
                oldbeer = self.beerTab[tmp].copy()
                print('Removing ' + tmp)
                self.beerTab.pop(tmp, None)
                
                with open(txtFile, 'a+') as f:
                    #json.dump(f,tab)
                    #listKeys = list(tab.keys())
                    #for item in sorted(listKeys):
                    f.write(str(tmp) + "\t" +
                        "\t".join(str(i) for i in oldbeer) + '\n')

                self.writeAllTabsToFiles()
                print('Done\n')
                return
            #else the beer is going to be deleted(for now)
            
        
        oldbeer = self.beerTab[tmp].copy()
        self.beerTab.pop(tmp, None)

        if delete:
            self.writeAllTabsToFiles()
            return

        self.addBeer(tmp, oldbeer)
        return
 
