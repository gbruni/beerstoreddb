# BeerStoredDB

Python scripts used to store information about owned beers in a simple
sort-of database.

With this it is possible to:
- add/edit/remove beers
- organise beers according to best before date, so that you know it's time
  to drink some of them
- organise beers according to bought date, so that you know it's time to
  drink some of them
- display a random beer, in case you don't know what to drink
- display how many beers you own
- display beers made by a producer
- display beers accordingly to a style

All the information are stored in simple text files, since I don't not
expect to own hundreds of thousands of beers.

ToDos:
- complete code comments/documentation
- when adding a new beer, first check among the already drunk ones
